import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { MoviesListComponent } from './movies-list/movies-list.component';

const routes: Routes = [
    {path: '', redirectTo: 'movies-list', pathMatch: 'full'},
    {path: 'movies-list',component:MoviesListComponent}   
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  
export class MoviesRoutingModule {
}
