import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  public movies:String[]
  constructor() {
    this.movies=["test1", "test2", "test3","test4"]
   }

  ngOnInit(): void {
  }

}
