import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { MainComponent } from './main/main.component';

const routes: Routes = [{
    path: '', component: MainComponent,
    children: [
        {path: 'movies',  loadChildren: () => import('./movies/movies.module').then( m => m.MoviesModule)}
        
    ]
}]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class DashboardRoutingModule {
  }
  